// Activity:
// 1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
const num = 2;
const getCube = Math.pow(num, 3);
console.log(`The cube of ${num} is ${getCube}`);

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.
const address = ['258 Washington Ave NW', 'California', 90011];
const [street, city, zipCode] = address;

console.log(`I live at ${street}, ${city} ${zipCode}`)

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

const animal = {
    animalName: "Lolong",
    animalType: "saltwater crocodile",
    weight: 1075,
    measureFt: 20,
    measureIn: 3
};

// Object Destructuring
const { animalName, animalType, weight, measureFt, measureIn } = animal;

console.log(`${animalName} was a ${animalType}. He weighed at ${weight} kgs with a measurement of ${measureFt} ft ${measureIn} in.`);

// function getFullName ({ givenName, maidenName, familyName}) {
//     console.log(`${ givenName } ${ maidenName } ${ familyName }`);
// }

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

const numbers = [1,2,3,4,5];
let sum = 0;
numbers.forEach(function(numbers){
	console.log(`${numbers}`);
});
for (value of numbers){
	sum += value;
}
console.log(sum);

// numbers.reduce((accumulator, currentValue) => {
// 	return accumulator + currentValue;
// });
// console.log(numbers);
// console.log(numbers += numbers);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog("Frankie", 3, "Golden Retriever");

console.log(myDog);


// 14. Create a git repository named S24.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.

